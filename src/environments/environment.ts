// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.



/*

    mi_argentina_authorize​: 'https://staging-id.argentina.gob.ar/'
    mi_argentina_token​: 'https://​staging​-id.argentina.gob.ar/token/'
    mi_argentina_user_info​: 'https://​staging​-id.argentina.gob.ar/userinfo/'
    mi_argentina_redirect_uri: 'https://dev-reservapasajes.cnrt.gob.ar/web/ingresar'
    mi_argentina_client_id: 445869
    mi_argentina_secret_id: 7dff55df3e92ec94b4c85907497229d0016b7a53f0f141aa04c7a0a2

    o único que tenes que tener en cuenta 
    que no es Code Flow, sino que es Implicit Flow

*/
export const environment = {
  production: false,
  entorno: 'LOCAL',
  nroAplicacion: 77,
  api_symfonybase_url: 'https://127.0.0.1:8000',
  log_level: 5,
  googleAnalyticsKey: false,
  redirect_uri: 'https://localhost:4200',
  current_idp: 'mi-argentina',
  authConfig: {
    issuer: 'https://staging-id.argentina.gob.ar',
    clientId: '445869',
    customUserinfoEndpoint: 'https://staging-id.argentina.gob.ar/userinfo/',
  },
//redirect_uri: 'http://localhost:4200',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
